﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter {
    public class GameManager : MonoBehaviour
    {
        private void Start()
        {
            Time.timeScale = 1;
        }
        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        public void Continue()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
