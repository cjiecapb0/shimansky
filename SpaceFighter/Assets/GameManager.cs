﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter {
    public class GameManager : MonoBehaviour
    {
        public GameObject gameOver;
        void Update()
        {
            if (Time.timeScale == 0)
                gameOver.SetActive(true);
        }
        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        public void Continue()
        {
            Time.timeScale = 1;
            gameOver.SetActive(false);
        }
    }
}
